// Теоретичне питання:
// Чому для роботи з input не рекомендується використовувати клавіатуру?
// Потому что для этого есть специальное событие input, и его использовать более логично, так как кроме ввода с клавиатуры, пользователь может вводить информацию в input не только с клавиатурой(например копировать,встравлять что-то, либо использовать голосовой вводы т.д. )
let buttons = document.querySelectorAll(".btn");

function changeColor(event) {
  switch (event.keyCode) {
    case 13:
      Array.from(buttons).map((item) => (item.style.background = "black"));
      buttons[0].style.background = "blue";
      break;
    case 83:
      Array.from(buttons).map((item) => (item.style.background = "black"));
      buttons[1].style.background = "blue";
      break;
    case 69:
      Array.from(buttons).map((item) => (item.style.background = "black"));
      buttons[2].style.background = "blue";
      break;
    case 79:
      Array.from(buttons).map((item) => (item.style.background = "black"));
      buttons[3].style.background = "blue";
      break;
    case 78:
      Array.from(buttons).map((item) => (item.style.background = "black"));
      buttons[4].style.background = "blue";
      break;
    case 76:
      Array.from(buttons).map((item) => (item.style.background = "black"));
      buttons[5].style.background = "blue";
      break;
    case 90:
      Array.from(buttons).map((item) => (item.style.background = "black"));
      buttons[6].style.background = "blue";
      break;
  }
}
document.addEventListener("keydown", changeColor);
